USE [master]
GO
/****** Object:  Database [shoplong]    Script Date: 6/16/2017 6:00:42 PM ******/
CREATE DATABASE [shoplong]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'shoplong', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL\MSSQL\DATA\shoplong.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'shoplong_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL\MSSQL\DATA\shoplong_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [shoplong] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [shoplong].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [shoplong] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [shoplong] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [shoplong] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [shoplong] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [shoplong] SET ARITHABORT OFF 
GO
ALTER DATABASE [shoplong] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [shoplong] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [shoplong] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [shoplong] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [shoplong] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [shoplong] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [shoplong] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [shoplong] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [shoplong] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [shoplong] SET  ENABLE_BROKER 
GO
ALTER DATABASE [shoplong] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [shoplong] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [shoplong] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [shoplong] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [shoplong] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [shoplong] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [shoplong] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [shoplong] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [shoplong] SET  MULTI_USER 
GO
ALTER DATABASE [shoplong] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [shoplong] SET DB_CHAINING OFF 
GO
ALTER DATABASE [shoplong] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [shoplong] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [shoplong] SET DELAYED_DURABILITY = DISABLED 
GO
USE [shoplong]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BINHLUAN]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BINHLUAN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenTaiKhoan] [nvarchar](50) NULL,
	[MaSanPham] [float] NULL,
	[NoiDung] [nvarchar](100) NULL,
	[NgayTao] [datetime] NULL CONSTRAINT [DF_BINHLUAN_NgayTao]  DEFAULT (getdate()),
 CONSTRAINT [PK_BINHLUAN] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CHITIETHD]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CHITIETHD](
	[MaChiTietHD] [int] IDENTITY(1,1) NOT NULL,
	[MaHoaDon] [nvarchar](50) NOT NULL,
	[MaSP] [int] NULL,
	[GiaSP] [float] NOT NULL,
	[SoLuong] [int] NOT NULL,
 CONSTRAINT [PK_CHITIETHD] PRIMARY KEY CLUSTERED 
(
	[MaChiTietHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HOADON]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HOADON](
	[MaHoaDon] [nvarchar](50) NOT NULL,
	[NgayLap] [datetime] NOT NULL CONSTRAINT [DF_HOADON_NgayLap]  DEFAULT (getdate()),
	[MaKhachHang] [nvarchar](128) NULL,
	[TrangThaiHD] [int] NOT NULL CONSTRAINT [DF_HOADON_TrangThaiHD]  DEFAULT ((1)),
	[TongTien] [float] NULL,
	[NoiNhan] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_HOADON] PRIMARY KEY CLUSTERED 
(
	[MaHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LOAISANPHAM]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LOAISANPHAM](
	[MaLoaiSanPham] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiSanPham] [nvarchar](50) NULL,
	[TrangThaiLSP] [int] NULL CONSTRAINT [DF_NHASANXUAT_TrangThaiNSX]  DEFAULT ((1)),
 CONSTRAINT [PK_NHASANXUAT] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NHASANXUAT]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NHASANXUAT](
	[MaNSX] [int] IDENTITY(1,1) NOT NULL,
	[TenNSX] [nvarchar](255) NULL,
	[TrangThaiNSX] [int] NULL CONSTRAINT [DF_LOAISANPHAM_TrangThaiLSP]  DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[MaNSX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SANPHAM]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SANPHAM](
	[MaSanPham] [int] IDENTITY(1,1) NOT NULL,
	[TenSanPham] [nvarchar](50) NULL,
	[Gia] [float] NULL,
	[GiamGia] [float] NULL,
	[HinhAnh] [nvarchar](100) NULL,
	[SoLuongBan] [int] NULL CONSTRAINT [DF_SANPHAM_SoLuongBan]  DEFAULT ((0)),
	[SoLuongXem] [int] NULL CONSTRAINT [DF_SANPHAM_SoLuongXem]  DEFAULT ((0)),
	[SoLuongDat] [int] NULL CONSTRAINT [DF_SANPHAM_SoLuongDat]  DEFAULT ((0)),
	[MoTa] [nvarchar](256) NULL,
	[LoaiSp] [int] NULL,
	[NhaSx] [int] NULL,
	[NgayTao] [datetime] NULL CONSTRAINT [DF_SANPHAM_NgayTao]  DEFAULT (getdate()),
	[TrangThaiSP] [int] NULL CONSTRAINT [DF_SANPHAM_TrangThaiSP]  DEFAULT ((1)),
 CONSTRAINT [PK__SANPHAM__FAC7442DB4657BE9] PRIMARY KEY CLUSTERED 
(
	[MaSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[v_giohang]    Script Date: 6/16/2017 6:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_giohang]
AS
SELECT        dbo.GioHang.*, dbo.SANPHAM.TenSanPham
FROM            dbo.GioHang INNER JOIN
                         dbo.SANPHAM ON dbo.GioHang.MaSanPham = dbo.SANPHAM.MaSanPham


GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201705090935071_InitialCreate', N'Shop.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EDC36107D2FD07F10F4D416CECA9726488DDD04CEDA6E8DC617649DA06F0157E2AE8548942A518E8DA25FD6877E527FA14389BAF1A2CBAEBCBB0E0A1459717866381C92C3D191FFFBE7DFF1DB07DF33EE7114BB01999807A37DD3C0C40E1C972C276642172F5E9B6FDF7CFFDDF8CCF11F8C4FB9DC1193839E249E98779486C79615DB77D847F1C877ED288883051DD9816F2127B00EF7F77FB10E0E2C0C10266019C6F84342A8EBE3F407FC9C06C4C6214D90771938D88BF9736899A5A8C615F2711C221B4FCCD95D108E3231D338F15C0426CCB0B7300D4448401105038F3FC67846A3802C67213C40DEED6388416E81BC1873C38F4BF1AE63D83F6463B0CA8E39949DC434F07B021E1C71A75862F7955C6B164E03B79D817BE9231B75EABA8979E1E0F4D187C00307880A8FA75EC48427E665A1E2240EAF301DE51D4719E47904705F83E8CBA88AB86774EEB75704D1E1689FFDB7674C138F26119E109CD008797BC64D32F75CFB77FC781B7CC1647274305F1CBD7EF90A3947AF7EC6472FAB2385B1825CED013CBA89821047601B5E14E3370DABDECF123B16DD2A7D32AF402CC17A308D4BF4F01E9325BD839572F8DA34CEDD07ECE44F78707D242E2C1FE844A3047E5E259E87E61E2EDAAD469DECFF0D5A0F5FBE1A44EB15BA7797E9D40BFA61E144B0AE3E602F6D8DEFDC305B5EB5F9FECCC5CEA3C067BFEBF195B57E9E054964B3C1045A915B142D31AD5B37B6CAE0ED14D20C6AF8B0CE51773FB499A572782B45D980565909B98A4DAF86DCDEA7D5DB39E24EC210262F0D2DE691A680AB9C5223A1DB9EC11ACB6039E81A2C0406F12DEF7D673E72BD0136BF0E5A20E158B8918F8B51BE0B20D410E96DF30D8A6358FBCE6F28BE6B301DFE3980E9336C271184E48C223F7C726D377701C157893F6791BE395D834DCDEDD7E01CD93488CE08EBB536DEFBC0FE1224F48C38A788E28FD4CE01D9CF5BD7EF0E30883927B68DE3F81C82193BD300F2E91CF082D0A3C3DE706C67DA76F231F590EBABB30F610FFD9C8B9619885A42CA423462AA4CA4C9D4F7C1D225DD4CCD45F5A66612ADA672B1BEA632B06E967249BDA1A940AB9D99D460B95D3A43C3277729ECEE6777EB1DDEBABDA0E2C619EC90F8574C7004DB98738328C5112967A0CBBEB18D64219D3EA6F4C9CFA654D327E42543AB5A6935A49BC0F0AB2185DDFDD5909A098FEF5D8765251DAE3CB930C0779257DFA6DAD79C60D9A697436D989B56BE993D40B75C4EE238B0DD7415288A5DBC5451B71F7238A3BD6E918D46AC7DC0C020D05D76E4C113189B2906D53539C51EA6D838B1B362E014C536726437C2809C1E86E527AAC2B0B2065237EE274927443A8E5827C42E4131AC5497507959B8C47643E4B57A49E8D9F10863632F74882DA738C484296CF54417E5EA920733A0D0234C4A9B87C65625E29A035193B5EAE6BC2D852DE75DAA446C24265B72674D5CF2FCED4902B3D9631B08CE66977431405BBEDB4680F2BB4AD700102F2EBB16A0C28D4913A03CA5DA4880D63DB68500ADBBE4D905687645ED3AFFC27D75D7C2B37E51DEFCB1DEE8AE2DC466CD1F3B169A59EE097D28F4C0911C9EA773D6881FA8E2720676F2FB59CC535D314418F80CD37AC9A6CC779579A8D50C220651136019682DA0FCC59F04242DA81EC6E5B5BC46EB7816D10336AFBB35C2F2BD5F80ADC4808C5D7D015A11D4BF261583B3D3EDA31859110D529077BA2C54701401216E5EF58177708AAE2E2B3BA64B2EDC271BAE0C8C4F4683835A32578D93F2C10CEEA53C34DBBDA44AC8FAA4646B7949489F345ECA0733B897788CB63B499114F4480BD67251FD081F68B1E5958EE2B429DAC6564688E20FC696863935BE4461E892658549C59F18B38C46357D31EB4F33F2330CCB8E156CA3C2DA42130D22B4C4422BA8064BCFDD28A6A788A23962759EA9E34B62CAB355B3FDE72AABC7A73C89F939904BB37FF337AB95D7F5B54356CE4278E773189ACF5299B47EAE987875778351DA90872245C97E1A78894FF49995BE77F6E2AEDA3F7B22238C2DC17E297392DC24E5B7759F779A117935AC3B3B45C6B2FA0CE921747ECEF3CDAAA77539A81E252F4955517465AAADCD982E75E9364B623AD87F925A119E6625710E4A15803FEA8951A131486095B6EEA875A64915B3DED21D51A093542185A61E565649233523AB0D2BE1693CAA96E8AE41A68954D1E5D6EEC80AC248155AD1BC02B6C266B1AD3BAA82535205563477C72E0926E2EEB9C36795F692D2FFB0CA2EB0EB9D561A8CA7D90A8739EC2AEFE9AB4095C73DB1F89B78098C3FDFC930D2DEE2FA875156B0582F8C3418FABDA6F66ABBBED534BE8FD763D6DE57D7B6F3A6F7F57ABC7EC1FAA42121DDDE4491427B718B136E6B637E736AFF1846BA4A6522A691BB1102E931A6D81F3181D1EC4F6FEAB9986DDCB9C02522EE02C734E3689887FB0787C26735BBF3898B15C78EA7B879EABE73A9CFD906E856E41E45F61D8A64F2C31A9F8194A0525DF98238F86162FE95F63A4E4B14EC5FE9E33DE322FE48DC3F1368B88D126CFC2D933987A1C537DFA776F42386EE5EBDF8E373D675CFB88E60C51C1BFB822F5799E1FAA70DBDACC9BAAE61CDCA1F3C3CDF0555FBB640892A2C88D53F2598BB7490CF08722B7FF0D1C38F7D4D537E2AB016A2E27380A1F00671A18EEEBF0A9696EAEFC04F9A52FDFB0D564DFD5FC5342DEDDF25FDC144D27FF76D28EFB9C5A3467119DAC49694FAB99534BD168372DB6793C4AD5E6BA1CBFCE91E706B70A457888C67462F1EEC7454B08707C3DE66683F3965785758C2257F63BBE4E04DF2811BDE007D5334E01D20AE298838DB27FB6E3AD67405DC1D674CF6A3F4EE58B0717AD6F689BB9B0E365D9977C783AD173D77C7626D5BE7E79623ADF311BA75B2ADCC1BD2BC8851D582DBC8B459E11C6EF8F3008220CB28B36F20D5ECAD26E6698BC25244AF544F1B13154B0B47D22B4934ABED37567EE0370E96CB34ABD5902D9B74F3FDBF51379769D6ADA1306E8306AC2411AAA8D92DFB5813DFE939D17E6B23696199B7E5AC8D6FD59F13CB7710A7D4568FE61DF1F321F50EE29221974E0F12AFFCBA17CECECA5F4884F33B76972504FB7B8904DBB553B390B9208B203FBC058B7211A14273892972E0483D89A8BB4036856656634E3FE24EEB76EC4DC71C3B17E43AA1614261C8D89F7BB582174B029AF4A74CE5BACDE3EB30FD7B24430C01CC74596DFE9ABC4B5CCF29EC3E57D48434102CBBE0155D3697945576978F05D255403A0271F71549D12DF6430FC0E26B3243F77815DB20FCDEE325B21FCB0AA00EA47D22EA6E1F9FBA6819213FE618657FF80931ECF80F6FFE07074A2F7228540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'41c3dc69-4bdb-4582-8bb3-5385e07d1d42', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'9cac9cb8-c8de-4585-89db-d54ea3863c80', N'Manager')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b236e1ef-06ef-4b74-b517-b1363c6c7159', N'41c3dc69-4bdb-4582-8bb3-5385e07d1d42')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2d0db6f5-c671-41a9-b9cd-ca128b1fb258', N'khang@gmail.com', 0, N'AG/FmRUels6D4vxmYA/OdCDgytbPW1QLQQ7RvYCvMuHxQj7OqGjQ0Gz6uxtBi7S5jA==', N'45733b69-5b41-4bf4-b901-d48dc3e5537b', NULL, 0, 0, NULL, 1, 0, N'khang@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b236e1ef-06ef-4b74-b517-b1363c6c7159', N'admin@gmail.com', 0, N'AEGPtvvqCefdzq1lM3UMIvhNmXA4kDIEqKUlUIOCbBhH3ScOB51ULeG5Vpnpy2c8ng==', N'f2d23703-f6df-4b83-9f09-8a1946fbd9e0', NULL, 0, 0, NULL, 0, 0, N'admin@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b5ba851c-264b-4d3f-89bd-4690d685b6d7', N'longnguyen476@gmail.com', 0, N'ACiI0MJcekwiiaL80PKmOr5DLHP9Ziay/tWfk9CxxOu4M6C4u6VbMKTFRSg+1iQjUQ==', N'4ccf7902-0d48-4f5c-bbe5-29aa016ca293', NULL, 0, 0, NULL, 1, 0, N'longnguyen476@gmail.com')
SET IDENTITY_INSERT [dbo].[BINHLUAN] ON 

INSERT [dbo].[BINHLUAN] ([id], [TenTaiKhoan], [MaSanPham], [NoiDung], [NgayTao]) VALUES (1, N'longnguyen476@gmail.com', 14, N'ffrgthyuj', NULL)
SET IDENTITY_INSERT [dbo].[BINHLUAN] OFF
SET IDENTITY_INSERT [dbo].[CHITIETHD] ON 

INSERT [dbo].[CHITIETHD] ([MaChiTietHD], [MaHoaDon], [MaSP], [GiaSP], [SoLuong]) VALUES (1, N'22401114062017b236e1ef-06ef-4b74-b517-b1363c6c7159', 1, 250000, 1)
SET IDENTITY_INSERT [dbo].[CHITIETHD] OFF
INSERT [dbo].[HOADON] ([MaHoaDon], [NgayLap], [MaKhachHang], [TrangThaiHD], [TongTien], [NoiNhan]) VALUES (N'22401114062017b236e1ef-06ef-4b74-b517-b1363c6c7159', CAST(N'2017-06-14 11:40:22.293' AS DateTime), N'b236e1ef-06ef-4b74-b517-b1363c6c7159', 1, 250000, N'an khê')
SET IDENTITY_INSERT [dbo].[LOAISANPHAM] ON 

INSERT [dbo].[LOAISANPHAM] ([MaLoaiSanPham], [TenLoaiSanPham], [TrangThaiLSP]) VALUES (1, N'Áo - Áo Khoác', 1)
INSERT [dbo].[LOAISANPHAM] ([MaLoaiSanPham], [TenLoaiSanPham], [TrangThaiLSP]) VALUES (2, N'Quần', 1)
INSERT [dbo].[LOAISANPHAM] ([MaLoaiSanPham], [TenLoaiSanPham], [TrangThaiLSP]) VALUES (3, N'Mũ', 1)
INSERT [dbo].[LOAISANPHAM] ([MaLoaiSanPham], [TenLoaiSanPham], [TrangThaiLSP]) VALUES (4, N'Giày - Balo', 1)
SET IDENTITY_INSERT [dbo].[LOAISANPHAM] OFF
SET IDENTITY_INSERT [dbo].[NHASANXUAT] ON 

INSERT [dbo].[NHASANXUAT] ([MaNSX], [TenNSX], [TrangThaiNSX]) VALUES (4, N'ADACHI', 1)
INSERT [dbo].[NHASANXUAT] ([MaNSX], [TenNSX], [TrangThaiNSX]) VALUES (5, N'MaBư', 1)
INSERT [dbo].[NHASANXUAT] ([MaNSX], [TenNSX], [TrangThaiNSX]) VALUES (6, N'NO Style', 1)
INSERT [dbo].[NHASANXUAT] ([MaNSX], [TenNSX], [TrangThaiNSX]) VALUES (7, N'Kirimaru', 1)
SET IDENTITY_INSERT [dbo].[NHASANXUAT] OFF
SET IDENTITY_INSERT [dbo].[SANPHAM] ON 

INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (1, N'Quần Jogger xanh', 300000, 250000, N'img/b5641065-e947-4cc5-8b1d-2820fe4820e56a77c9a8-65a8-7100-099b-00132237c111.jpg', 0, 0, 0, N'<p>đẹp</p>', 2, 5, CAST(N'2017-06-12 19:20:10.410' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (2, N'Áo thun sọc xanh đen', 150000, 120000, N'img/cd6628e7-9c51-441d-ae5d-eba9267eae480c0d80c4-2a25-4d00-3073-00137635cf40.jpg', 0, 0, 0, NULL, 1, 4, CAST(N'2017-06-12 19:21:53.150' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (3, N'Quần Jogger đen', 300000, 250000, N'img/55fe8526-bec5-40ca-82a1-1d2fa8b5ec857a910f60-9cd2-1100-1ca3-00135c4db287.jpg', 0, 0, 0, NULL, 2, 5, CAST(N'2017-06-12 19:22:49.587' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (4, N'Balo Xám', 250000, 200000, N'img/8133e137-b0a5-4c17-a380-a29407a1b8e48c8ad4d2-0299-1100-a2d4-001301d5db0c.jpg', 0, 0, 0, NULL, 4, 6, CAST(N'2017-06-12 19:23:15.003' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (5, N'Áo họa tiết 1', 150000, 120000, N'img/8643ca90-118d-461b-aec1-465e70581fc215ed0874-ea4d-7800-551b-00133e866adf.jpg', 0, 0, 0, NULL, 1, 7, CAST(N'2017-06-12 19:24:09.510' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (6, N'Áo thun xám', 120000, 100000, N'img/c0671d91-4dfc-4282-b376-016e362b6fa189d56f56-0d27-1400-1238-0013118b4fad.jpg', 0, 0, 0, NULL, 1, 6, CAST(N'2017-06-12 19:24:29.097' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (7, N'áo thun xọc trắng', 180000, 150000, N'img/ec82f561-6fcd-468a-a371-44f46665b618221f37f3-18b4-0f00-fb45-00133e4d9a95.jpg', 0, 0, 0, NULL, 1, 4, CAST(N'2017-06-12 19:24:54.707' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (8, N'Balo Đỏ', 400000, 200000, N'img/9ee5afa2-7f4b-4c72-9c78-97bd3e39565b561dcba2-da70-0100-d26e-0013390f5afe.jpg', 0, 0, 0, NULL, 4, 6, CAST(N'2017-06-12 19:25:23.607' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (9, N'Giày quai hậu', 300000, 180000, N'img/247c850e-a533-4ce6-9f11-0c562ded9a322831cc9b-a0b5-3900-e6ea-0013173e3b08.jpg', 0, 0, 0, NULL, 4, 7, CAST(N'2017-06-12 19:25:56.187' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (10, N'Quần jean nữ', 350000, 300000, N'img/e3c84f90-1e38-4e30-8506-23f4a84c01f34321d723-e736-0400-1587-00130f2c0edf.jpg', 0, 0, 0, NULL, 2, 6, CAST(N'2017-06-12 19:26:46.930' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (11, N'Balo đen', 300000, 200000, N'img/163b9c1f-9702-45ff-9a87-f7dcdd8b86a3012065ef-e177-2700-b644-0012c3d9d5ae.jpg', 0, 0, 0, NULL, 4, 5, CAST(N'2017-06-12 19:27:07.863' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (12, N'Quần Short Jean', 300000, 250000, N'img/961dfe53-7335-46e7-9cdb-e7c75ddd42d3a50dfb15-43c8-0100-69af-001221105367.jpg', 0, 0, 0, NULL, 2, 6, CAST(N'2017-06-12 19:28:37.263' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (13, N'Áo khoác đen', 400000, 300000, N'img/a0df2288-eb71-4053-ab5f-f4484a414df7c0c6d978-49f8-3b00-e662-001381ed562f.jpg', 0, 0, 0, NULL, 1, 7, CAST(N'2017-06-12 19:29:04.350' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (14, N'Áo thun xanh đen', 150000, 120000, N'img/f708501e-b203-4c61-b377-56bd3c64d3ebcdd6944e-d19b-2100-1a6f-0012eb08795f.jpg', 0, 0, 0, NULL, 1, 4, CAST(N'2017-06-12 19:29:41.277' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (15, N'Áo thun đen dài tay', 150000, 100000, N'img/12209bcf-d079-4340-b07f-f12521aaf5a4d4a5ca6d-76f6-0100-f87e-0013806ad374.jpg', 0, 0, 0, NULL, 1, 4, CAST(N'2017-06-12 19:30:08.127' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (16, N'Áo khoác xanh', 400000, 300000, N'img/b577bf12-e0dc-46ac-964c-18e366c230f9d6365f49-3551-1c00-726d-0012e358ad07.jpg', 0, 0, 0, NULL, 1, 5, CAST(N'2017-06-12 19:30:27.613' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (17, N'Quần jean nữ bạc', 400000, 300000, N'img/5eb770c7-b7b0-4ae7-a049-a80f82665497e1b76ea3-4a56-9e00-e930-00138535a006.jpg', 0, 0, 0, NULL, 2, 4, CAST(N'2017-06-12 19:30:55.580' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (18, N'Áo khoác hồng', 250000, 200000, N'img/ec6dd572-5a55-4cd2-b929-50c863bf1730e4f244ca-2c70-8100-64b5-0013852bda23.jpg', 0, 0, 0, NULL, 1, 7, CAST(N'2017-06-12 19:32:13.333' AS DateTime), 1)
INSERT [dbo].[SANPHAM] ([MaSanPham], [TenSanPham], [Gia], [GiamGia], [HinhAnh], [SoLuongBan], [SoLuongXem], [SoLuongDat], [MoTa], [LoaiSp], [NhaSx], [NgayTao], [TrangThaiSP]) VALUES (19, N'Áo thun xám', 150000, 120000, N'img/0f537cfd-cb63-4630-9866-047efca980a3eaaaeb4d-0ad4-3100-220d-0013561e2199.jpg', 0, 0, 0, NULL, 1, 6, CAST(N'2017-06-12 19:32:32.397' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[SANPHAM] OFF
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CHITIETHD]  WITH CHECK ADD  CONSTRAINT [FK_CHITIETHD_HOADON] FOREIGN KEY([MaHoaDon])
REFERENCES [dbo].[HOADON] ([MaHoaDon])
GO
ALTER TABLE [dbo].[CHITIETHD] CHECK CONSTRAINT [FK_CHITIETHD_HOADON]
GO
ALTER TABLE [dbo].[CHITIETHD]  WITH CHECK ADD  CONSTRAINT [FK_CHITIETHD_SANPHAM] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SANPHAM] ([MaSanPham])
GO
ALTER TABLE [dbo].[CHITIETHD] CHECK CONSTRAINT [FK_CHITIETHD_SANPHAM]
GO
ALTER TABLE [dbo].[SANPHAM]  WITH CHECK ADD  CONSTRAINT [FK_SANPHAM_LOAISANPHAM] FOREIGN KEY([LoaiSp])
REFERENCES [dbo].[LOAISANPHAM] ([MaLoaiSanPham])
GO
ALTER TABLE [dbo].[SANPHAM] CHECK CONSTRAINT [FK_SANPHAM_LOAISANPHAM]
GO
ALTER TABLE [dbo].[SANPHAM]  WITH CHECK ADD  CONSTRAINT [FK_SANPHAM_NHASANXUAT] FOREIGN KEY([NhaSx])
REFERENCES [dbo].[NHASANXUAT] ([MaNSX])
GO
ALTER TABLE [dbo].[SANPHAM] CHECK CONSTRAINT [FK_SANPHAM_NHASANXUAT]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "GioHang"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SANPHAM"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_giohang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_giohang'
GO
USE [master]
GO
ALTER DATABASE [shoplong] SET  READ_WRITE 
GO
