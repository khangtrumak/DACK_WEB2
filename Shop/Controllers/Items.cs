﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shop;

namespace Shop.Controllers
{
    public class Items
    {
        private SANPHAM sp = new SANPHAM();
        private int soluong;

        public SANPHAM Sp
        {
            get
            {
                return sp;
            }

            set
            {
                this.sp = value;
            }
        }

        public int Soluong
        {
            get
            {
                return soluong;
            }

            set
            {
                soluong = value;
            }
        }

        public Items()
        { }
        public Items(SANPHAM sp, int sl)
        {
            this.sp = sp;
            this.soluong = sl;
        }
    }
}