﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Shop.Controllers
{
    public class BinhLuanController : Controller
    {
        // GET: BinhLuan
        [Authorize]
        public ActionResult Create(int MaSanPham, String NoiDung)
        {
            Shop.Models.BinhLuanBus.BinhLuanBus.insert(NoiDung, MaSanPham, User.Identity.Name);
            return RedirectToAction("Details", "Product", new { id = MaSanPham });
        }
    }
}