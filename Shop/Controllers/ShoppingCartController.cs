﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using shop;
using Microsoft.AspNet.Identity;
namespace Shop.Controllers
{
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        public ActionResult Index()
        {
            if (Session["cart"] != null)
            {
                ViewBag.tongtien = TongTien();
            }
            return View();
        }
        private int isExit(int id)
        {
            List<Items> cart = (List<Items>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Sp.MaSanPham == id)
                {
                    return i;
                }
            }
            return -1;

        }
        private float TongTien()
        {
            List<Items> cart = (List<Items>)Session["cart"];
            float tongtien = 0;
            for (int i = 0; i < cart.Count; i++)
            {
                tongtien = tongtien + (float)(cart[i].Sp.GiamGia * cart[i].Soluong);
            }
            return tongtien;

        }

        public ActionResult Them(int id,int soluong)
        {
            SANPHAM sp = Models.SanPhamBus.SanPhamBus.GetProduct(id);
            if (Session["cart"]==null)
            {
                if(soluong <= 0)
                {
                    return RedirectToAction("Index");
                }
                List<Items> cart = new List<Items>();
                Items temp = new Items(sp, soluong);
                cart.Add(temp);
                Session["cart"] = cart;
            }
            else
            {
                List<Items> cart = (List<Items>)Session["cart"];
                int result = isExit(id);
                if (result == -1)
                {
                    if (soluong <= 0)
                    {
                        return RedirectToAction("Index");
                    }
                    Items temp = new Items(sp, soluong);
                    cart.Add(temp);
                }
                else
                {
                    if ((cart[result].Soluong + soluong) <= 0)
                    {
                        return RedirectToAction("Xoa",new { id = id});
                    }
                    cart[result].Soluong = cart[result].Soluong + soluong;
                    
                }
                Session["cart"] = cart;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Update(int id, int soluong)
        {
            SANPHAM sp = Models.SanPhamBus.SanPhamBus.GetProduct(id);
            if (Session["cart"] == null)
            {
                if (soluong <= 0)
                {
                    return RedirectToAction("Index");
                }
                List<Items> cart = new List<Items>();
                Items temp = new Items(sp, soluong);
                cart.Add(temp);
                Session["cart"] = cart;
            }
            else
            {
                List<Items> cart = (List<Items>)Session["cart"];
                int result = isExit(id);
                if (result == -1)
                {
                    if (soluong <= 0)
                    {
                        return RedirectToAction("Index");
                    }
                    Items temp = new Items(sp, soluong);
                    cart.Add(temp);
                }
                else
                {
                    if (soluong <= 0)
                    {
                        return RedirectToAction("Xoa", new { id = id });
                    }
                    cart[result].Soluong = soluong;

                }
                Session["cart"] = cart;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Xoa(int id)
        {
            List<Items> cart = (List<Items>)Session["cart"];
            int result = isExit(id);
           
            cart.RemoveAt(result);
            if(cart.Count==0)
            {
                Session["cart"] = null;
            }
            else
            {
                Session["cart"] = cart;
            }
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.name = User.Identity.Name;
            if (Session["cart"]==null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.tongtien = TongTien();
            return View();
        }
        [HttpPost]
        public ActionResult Create(string diachi)
        {
            if(diachi=="")
            { return View(); }
            string time = DateTime.Now.ToString("ssmmhhddMMyyyy");
            string id = User.Identity.GetUserId();
            string MaHD = time + id;
            float tongtien= TongTien();
            Models.GioHangBus.GioHangBus.inserthd(MaHD,id,tongtien,diachi);
            List<Items> cart = (List<Items>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                Models.GioHangBus.GioHangBus.insertchitiet(MaHD, cart[i]);
            }
            Session["cart"] = null;
            return RedirectToAction("Index");
        }

    }
}