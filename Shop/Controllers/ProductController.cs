﻿using System;
using Shop.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Models.BinhLuanBus;

namespace Shop.Controllers
{
    public class ProductController : Controller
    {
        // GET: SanPham
        public ActionResult Index()
        {
            ViewBag.TopView = Models.SanPhamBus.SanPhamBus.List();
            ViewBag.TopSell = Models.SanPhamBus.SanPhamBus.List2();
            return View();
        }
        public ActionResult LoaiSP(int id, int page = 1)
        {
            var list = Models.SanPhamBus.SanPhamBus.LoaiSP(page, 6, id);

            return View(list);
        }
        public ActionResult NSX(int id, int page = 1)
        {
            var list = Models.SanPhamBus.SanPhamBus.NSX(page, 6, id);

            return View(list);
        }

        // GET: SanPham/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.ListComments = BinhLuanBus.ListComments(id);
            return View(Models.SanPhamBus.SanPhamBus.GetProduct(id));
        }
        public ActionResult Search(string tim, int page = 1)
        {
            var list = Models.SanPhamBus.SanPhamBus.TimKiem(page, 6, tim);
            return View(list);
        }

        // GET: SanPham/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SanPham/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SanPham/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SanPham/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SanPham/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
