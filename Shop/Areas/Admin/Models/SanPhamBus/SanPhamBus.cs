﻿using shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Areas.Admin.Models.SanPhamBus
{
    public class SanPhamBus
    {
        public static IEnumerable<SANPHAM> List()
        {
            var db = new shopDB();
            return db.Query<SANPHAM>("select * from sanpham");
        }
        public static IEnumerable<SANPHAM> Top()
        {
            var db = new shopDB();
            return db.Query<SANPHAM>("select * from sanpham order by SoLuongBan desc" );
        }


        public static void insert(SANPHAM sp)
        {
            sp.NgayTao = DateTime.Now;
            sp.SoLuongBan = sp.SoLuongDat = sp.SoLuongXem = 0;
            sp.Insert();
        }

        //public static void Delete(int id)
        //{
        //        using (var db = new shopDB())
        //        {
        //            db.Execute("DELETE FROM SANPHAM WHERE MaSanPham = @0", id);
        //        }
        //}

        public static SANPHAM GetProduct(int id)
        {
            using (var db = new shopDB())
            {
                
                return db.SingleOrDefault<SANPHAM>("select * from SANPHAM where MaSanPham = @0", id);
            }
        }
        public static void UpdateProduct(SANPHAM Sp, int valueImg)
        {
            if (valueImg == 0)
            {
                using (var db = new shopDB())
                {
                    db.Update<SANPHAM>("SET TenSanPham=@0, Gia=@1, GiamGia=@2,MoTa=@3, LoaiSp=@4,NhaSx =@5 WHERE MaSanPham=@6", Sp.TenSanPham, Sp.Gia, Sp.GiamGia, Sp.MoTa, Sp.LoaiSp, Sp.NhaSx, Sp.MaSanPham);
                }
            }
            else
            {
                using (var db = new shopDB())
                {
                    db.Update<SANPHAM>("SET TenSanPham=@0, Gia=@1, GiamGia=@2,MoTa=@3, LoaiSp=@4,NhaSx =@5,HinhAnh = @6 WHERE MaSanPham=@7", Sp.TenSanPham, Sp.Gia, Sp.GiamGia, Sp.MoTa, Sp.LoaiSp, Sp.NhaSx,Sp.HinhAnh, Sp.MaSanPham);
                }
            }

        }
        public static void Lock(int id, int khoa)
        {
            if (khoa == 1)
            {
                using (var db = new shopDB())
                {
                    db.Update<SANPHAM>("SET TrangThaiSP=0 WHERE MaSanPham=@0", id);
                }
            }
            else
            {
                using (var db = new shopDB())
                {
                    db.Update<SANPHAM>("SET TrangThaiSP=1 WHERE MaSanPham=@0", id);
                }
            }
        }
    }
}