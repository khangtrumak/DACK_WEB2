﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shop;
namespace Shop.Areas.Admin.Models.NhaSanXuatBus
{
    public class NhaSanXuatBus
    {
        public static IEnumerable<NHASANXUAT> List()
        {
            var db = new shopDB();
            return db.Query<NHASANXUAT>("select * from NHASANXUAT");
        }
        public static IEnumerable<NHASANXUAT> Listnsx()
        {
            var db = new shopDB();
            return db.Query<NHASANXUAT>("select * from NHASANXUAT where TrangThaiNSX =1");
        }
        public static NHASANXUAT Get(int id)
        {
            using (var db = new shopDB())
            {
                return db.SingleOrDefault<NHASANXUAT>("select * from NHASANXUAT where MaNSX = @0", id);
            }
        }
        public static void Lock(int id, int khoa)
        {
            if (khoa == 1)
            {
                using (var db = new shopDB())
                {
                    db.Update<NHASANXUAT>("SET TrangThaiNSX=0 WHERE MaNSX=@0", id);
                }
            }
            else
            {
                using (var db = new shopDB())
                {
                    db.Update<NHASANXUAT>("SET TrangThaiNSX=1 WHERE MaNSX=@0", id);
                }
            }
        }
        public static void UpdateNSX(NHASANXUAT nsx)
        {
            using (var db = new shopDB())
            {
                db.Update<NHASANXUAT>("SET TenNSX=@0 WHERE MaNSX=@1", nsx.TenNSX, nsx.MaNSX);
            }
        }
        public static void Insert(NHASANXUAT nsx)
        {
            nsx.Insert();
        }
    }
}