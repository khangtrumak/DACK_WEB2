﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shop;
namespace Shop.Areas.Admin.Models.HoaDonBus
{
    public class HoaDonBus
    {
        public static IEnumerable<HOADON> List(int id)
        {
            var db = new shopDB();
            return db.Query<HOADON>("select * from hoadon where TrangThaiHD =@0", id);
        }
        public static HOADON Gethd(string mahd)
        {
            var db = new shopDB();
            return db.SingleOrDefault<HOADON>("select * from hoadon where MaHoaDon = @0", mahd);
        }
        public static IEnumerable<CHITIETHD> ListCT(string mahd)
        {
            var db = new shopDB();
            return db.Query<CHITIETHD>("select * from chitiethd where MaHoaDon =@0", mahd);
        }
        public static void Lock(string id, int khoa)
        {
            if (khoa == 1)
            {
                using (var db = new shopDB())
                {
                    db.Update<HOADON>("SET TrangThaiHD=2 WHERE MaHoaDon=@0", id);
                }
            }
            else
            {
                using (var db = new shopDB())
                {
                    db.Update<HOADON>("SET TrangThaiHD=3 WHERE MaHoaDon=@0", id);
                }
            }
        }
        public static void Update(shop.HOADON hd)
        {
            var db = new shopDB();
            db.Update<HOADON>("SET NoiNhan = @0 where MaHoaDon = @1", hd.NoiNhan, hd.MaHoaDon);
        }
    }
}