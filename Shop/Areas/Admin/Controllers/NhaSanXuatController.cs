﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NhaSanXuatController : Controller
    {
       
        // GET: Admin/NhaSanXuat
        public ActionResult Index()
        {
            var nhasanxuat = Models.NhaSanXuatBus.NhaSanXuatBus.List();
            ViewBag.dsnsx = nhasanxuat;
            return View();
        }

        // GET: Admin/NhaSanXuat/Details/5
       
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/NhaSanXuat/Create
       
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/NhaSanXuat/Create
        [HttpPost]
        public ActionResult Create(shop.NHASANXUAT nsx)
        {
            try
            {
                // TODO: Add insert logic here
                Models.NhaSanXuatBus.NhaSanXuatBus.Insert(nsx);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/NhaSanXuat/Edit/5
       
        public ActionResult Edit(int id)
        {
            return View(Models.NhaSanXuatBus.NhaSanXuatBus.Get(id));
        }

        // POST: Admin/NhaSanXuat/Edit/5
        [HttpPost]
        public ActionResult Edit(shop.NHASANXUAT nsx)
        {
            try
            {
                // TODO: Add update logic here
               Models.NhaSanXuatBus.NhaSanXuatBus.UpdateNSX(nsx);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/NhaSanXuat/Delete/5
       
        public ActionResult Delete(int id,int khoa)
        {
            Models.NhaSanXuatBus.NhaSanXuatBus.Lock(id, khoa);
            return RedirectToAction("Index");
        }

        // POST: Admin/NhaSanXuat/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
