﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HoaDonController : Controller
    {
        // GET: Admin/HoaDon
        public ActionResult Index(int ?id)
        {
            if(id == null)
            {
                return RedirectToAction("Index", new { id = 1 });
            }
            ViewBag.list = Models.HoaDonBus.HoaDonBus.List((int)id);
            return View();
        }
        public ActionResult Edit(string mahd)
        {
            ViewBag.chitiet = Models.HoaDonBus.HoaDonBus.ListCT(mahd);
            var hoadon = Models.HoaDonBus.HoaDonBus.Gethd(mahd);
            return View(hoadon);
        }
        [HttpPost]
        public ActionResult Edit(shop.HOADON hd)
        {
            Models.HoaDonBus.HoaDonBus.Update(hd);
            return RedirectToAction("Edit", new { mahd= hd.MaHoaDon });
        }
        public ActionResult Changes(string mahd, int khoa)
        {
            Models.HoaDonBus.HoaDonBus.Lock(mahd, khoa);
            return RedirectToAction("Index",new { id=khoa});
        }
    }
}