﻿using System;
using shop;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
namespace Shop.Models.SanPhamBus
{
    public class SanPhamBus
    {
        public static IEnumerable<SANPHAM> List()
        {
            var db = new shopDB();
            return db.Query<SANPHAM>("select top 3 * from sanpham where TrangThaiSP=1 order by SoLuongXem desc");
        }
        public static IEnumerable<SANPHAM> List2()
        {
            var db = new shopDB();
            return db.Query<SANPHAM>("select top 3 * from sanpham where TrangThaiSP=1 order by SoLuongBan desc");
        }
        public static SANPHAM GetProduct(int id)
        {
            var db = new shopDB();
            db.Update<SANPHAM>("SET SoLuongXem=SoLuongXem+1 where MaSanPham = @0 and TrangThaiSP=1", id);
            return db.SingleOrDefault<SANPHAM>("select * from sanpham where MaSanPham = @0 and TrangThaiSP=1", id);
        }


        public static Page<SANPHAM> LoaiSP(int pagenume, int itemperpage, int id)
        {
            var db = new shopDB();
            return db.Page<SANPHAM>(pagenume, itemperpage, "select * from sanpham where LoaiSP = @0 and TrangThaiSP=1", id);
        }
        public static Page<SANPHAM> NSX(int pagenume, int itemperpage, int id)
        {
            var db = new shopDB();
            return db.Page<SANPHAM>(pagenume, itemperpage, "select * from sanpham where NhaSx = @0 and TrangThaiSP=1", id);
        }

        public static Page<SANPHAM> TimKiem(int pagenume, int itemperpage,string name)
        {
            var db = new shopDB();
            return db.Page<SANPHAM>(pagenume, itemperpage, "SELECT * FROM SANPHAM WHERE TenSanPham LIKE @0 and TrangThaiSP=1", "%"+ name+"%");
        }
    }
  
   
}