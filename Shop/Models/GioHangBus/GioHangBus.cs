﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shop;
using Shop.Controllers;

namespace Shop.Models.GioHangBus
{
    public class GioHangBus
    {
        public static void inserthd(string MaHD, string MTK,float tongtien,string noinhan)
        {
            HOADON hd = new HOADON();
            hd.MaHoaDon = MaHD;
            hd.NgayLap = DateTime.Now;
            hd.MaKhachHang = MTK;
            hd.TrangThaiHD = 1;
            hd.TongTien = tongtien;
            hd.NoiNhan = noinhan;
            hd.Insert();
        }
        public static void insertchitiet(string MaHD,Items items)
        {
            CHITIETHD ct = new CHITIETHD();
            ct.MaHoaDon = MaHD;
            ct.MaSP = items.Sp.MaSanPham;
            ct.GiaSP = (double)items.Sp.GiamGia;
            ct.SoLuong = items.Soluong;
            ct.Insert();
        }
        public static IEnumerable<HOADON> ListHD(string tk)
        {
            var db = new shopDB();
            return db.Query<HOADON>("where MaKhachHang =@0", tk);
        }
    }
}