﻿using shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
namespace Shop.Models.ProductType
{
    public class ProductType
    {

        public static IEnumerable<LOAISANPHAM> danhsachlsp()
        {
            var db = new shopDB();
            return db.Query<LOAISANPHAM>("select * from loaisanpham where TrangThaiLSP = 1");
        }
        public static IEnumerable<NHASANXUAT> danhsachnsx()
        {
            var db = new shopDB();
            return db.Query<NHASANXUAT>("select * from NHASANXUAT where TrangThaiNSX = 1");
        }


    }
    
}